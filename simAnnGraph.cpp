#include<iostream>
#include<bits/stdc++.h>
#include <stdio.h>      
#include <stdlib.h>     
#include <time.h> 
#include <cmath>
#include <cmath>
#include <set>
#include <algorithm>
#include <fstream>
#include <string>
#include <cstring>
#include <time.h> 
using namespace std;

class Graph{
public:
    int N;
    int E;
    vector<vector<bool>> adjList;
    vector<int> colors;
    //broj grana, cvorovi, susedi/grane
    Graph(int N, int E, vector<int> U, vector<int> V){
        this->N=N;
        this->E=E;
        
        int a,b;
        adjList.resize(N);
        for(int j=0; j<N; j++){
            adjList[j].resize(N);
        }
        colors.resize(N);
        for(int i=0; i<N; i++){
            colors[i]=i+1;
        }
        for(int i=0;i<E;i++){
            a=U[i];
            b=V[i];
            a--; b--;
            adjList[a][b]=1;
            adjList[b][a]=1;
        }
    }
};

void printGraph(Graph graph){
    for(int i=0; i<graph.N; i++){
        for(int j=0; j<graph.N; j++){
            cout<<graph.adjList[i][j]<<" ";
        }
        cout<<endl;
    }
    cout<<endl;
    
    for(int i=0; i<graph.N; i++){
        cout<<graph.colors[i]<<" ";
    }
    cout<<endl;
}
bool isColoringValid(Graph graph){
    for(int i=0; i<graph.N; i++){
        for(int j=0; j<graph.N; j++){
            if(graph.adjList[i][j]==1 && graph.colors[i]==graph.colors[j] && graph.colors[i]!=0
                && graph.colors[j]!=0){
                    return false;
                }
        }
    }
    return true;
}
//fja cilja, vraca broj boja
int lossFunction(Graph graph){
    set<int> s;
    for(int i=0; i<graph.N; i++){
        s.insert(graph.colors[i]);
    }
    return s.size();
}
//fja koja boji cvor ili lokalno ili globalno
Graph assignColor(Graph graph, int node){
    int locORglob=((double) rand() / (RAND_MAX));
    if(locORglob<=0.5){
        //lokalno, ne ubacujemo nove boje, samo ona kojes su vec u grafu
        int colorIndex=rand() % graph.N;
        graph.colors[node]= graph.colors[colorIndex];
    }
    else{
        //globalno, "sve" boje dolaze u obzir
        int color=rand() % graph.N + 1;
        graph.colors[node]=color;
    }
    return graph;
}
void simulatedAnnealing(int maxIter, int sameTemp, Graph graph){
    for(int i=0; i<maxIter; i++){
        //za svaki cvor
        cout<<i+1<<endl;
        for(int j=0; j<graph.N; j++){
            //stare vrednosti
            int node=j;
            int oldColor=graph.colors[node];
            int oldValue=lossFunction(graph);
            
            for(int k=0; k<sameTemp; k++){
                graph=assignColor(graph, node);
                if(!isColoringValid(graph)){
                    graph.colors[node]=oldColor;
                    continue;
                }
                int newValue=lossFunction(graph);
                
                if (newValue<oldValue){
                    oldValue=newValue;
                }
                else{
                    double p=1.0/sqrt(i+1);
                    double q=((double) rand() / (RAND_MAX));
                    if(p>q){
                        oldValue=newValue; 
                    }
                    else{
                        graph.colors[node]=oldColor;
                    }
                }
            }
        }
    }
    printGraph(graph);
    cout<<"Broj boja: "<<lossFunction(graph)<<endl;
}

int main(int argc, char** argv){
    for (int i = 1; i < argc; ++i){
        cout<<argv[i]<<endl;
        
        ifstream myFile;
        myFile.open(argv[i], ios::in);
        string line;
        
        //ucitavanje za E i N
        getline(myFile, line);
        istringstream ss(line);  
        
        int E, N;
        string x;
        ss>>x>>E>>N;
        //ucitavanje grana
        vector<int> U;
        U.resize(E);
        vector<int> V;
        V.resize(E);
        int k=0;
        while(getline( myFile, line) && k<E){
            istringstream ss(line); 
            string x;
            ss>>x>>U[k]>>V[k];
            k++;
        }
        myFile.close();
        Graph graph(N, E, U, V);
        srand(time(0));
        simulatedAnnealing(1500, 10, graph);
    }
    
    return 0;
}
