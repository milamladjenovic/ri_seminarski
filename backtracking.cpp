#include <stdbool.h>
#include <stdio.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <fstream>
#include <string>
#include <cstring>
#include <sstream>
#include <time.h> 
using namespace std;

//broj cvorova
int V;

void printSolution(int color[]);

bool isSafe(
	int v, vector<vector<bool>> graph,
	int color[], int c)
{
    //za svaki cvor radi sledece
	for (int i = 0; i < V; i++)
        //ako postoji grana izmedju cvora v i cvora i
        //onda je to sused cvora v
        //i ako je njegova boja(color[i]) ista kao boja cvora v(c) onda stanje nije validno
		if (
			graph[v][i] && c == color[i])
			return false;
	return true;
}

bool graphColoringUtil(vector<vector<bool>> graph, int m,int color[], int v){
	//ako je cvor koji razmatramo poslednji cvor,tj. stigli smo do poslednjeg
    //nivoa u grafu pretrage, onda smo uspeli i obojili smo graf
	if (v == V)
		return true;

	//redom probaj boje za cvor v
	for (int c = 1; c <= m; c++) {
		//da li je boja bezbedna za cvor v
		if (isSafe(
				v, graph, color, c)) {
			color[v] = c;

			//oboj i druge cvorove
			if (
				graphColoringUtil(
					graph, m, color, v + 1)
				== true)
				return true;

			//nismo uspeli da obojimo ovaj cvor sa ovom bojom, resetujemo boju cvora v
			color[v] = 0;
		}
	}

    //ako ne mozemo obojiti cvor v ni sa jednom bojom vracamo false
	return false;
}

//fja vraca false ako graf ne moze biti obojen, i true ako postoji resenje
//fja vraca prvo resenje na koje naidje
bool graphColoring(
	vector<vector<bool>> graph, int m)
{
	//ppostavimo da nijedan cvor nema boju, sve su boje 0
	int color[V];
	for (int i = 0; i < V; i++)
		color[i] = 0;

	// Zovemo fju koja ce obojiti graf
	if (
		graphColoringUtil(
			graph, m, color, 0)
		== false) {
        //resenje ne postoji ako fja vrati false
		return false;
	}

	// Stampamo resenje
	printSolution(color);
	return true;
}
//funkcija koja stampa resenje
void printSolution(int color[])
{
    int max=-1;
	for (int i = 0; i < V; i++){
		cout<<i+1<<": "<<color[i]<<endl;
        if(color[i]>max){
            max=color[i];
        }
    }
	cout <<"\nPotreban broj boja je: "<<max<< "\n";
}
int main(int argc, char** argv){
    for (int i = 1; i < argc; ++i){
        cout<<argv[i]<<endl;
        
        ifstream myFile;
        myFile.open(argv[i], ios::in);
        string line;
        
        //ucitavanje za E i N
        getline(myFile, line);
        istringstream ss(line);  
        
        int E;
        string x;
        ss>>x>>E>>V;
        //ucitavanje grana
        vector<int> U1;
        U1.resize(E);
        vector<int> V1;
        V1.resize(E);
        int k=0;
        while(getline( myFile, line) && k<E){
            istringstream ss(line); 
            string x;
            ss>>x>>U1[k]>>V1[k];
            k++;
        }
        myFile.close();
        //pravimo matrixu susedstva
        vector<vector<bool>> graph;
        graph.resize(V);
        for(int j=0; j<V; j++){
            graph[j].resize(V);
        }
        int a,b;
        for(int i=0; i<E; i++){
            a=U1[i];
            b=V1[i];
            a--; b--;
            graph[a][b]=1;
            graph[b][a]=1;
        }
        int m = 1; // Broj boja
        while(!graphColoring(graph, m)){
            m+=1;
        }
    }
	return 0;
}
