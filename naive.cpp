#include<bits/stdc++.h>
#include <stdio.h>   
#include <stdlib.h>    
#include <time.h> 
using namespace std;

//broj cvorova u grafu
int V;

void printSolution(vector<int> color);

// proverava da li je obojeni graf korektan(bezbedan)
bool isSafe(vector<vector<bool>> graph, vector<int> color){
	// proveramo za svaki cvor
	for (int i = 0; i < V; i++){
		for (int j = i + 1; j < V; j++){
			if (graph[i][j] && color[j] == color[i])
				return false;}}
	return true;
}

bool graphColoring(vector<vector<bool>> graph, int m, int i, vector<int> color){
	// ako smo dostigli kraj (zavrsetak rekurzije)
	if (i == V) {
		// ako smo dobro obojili
		if (isSafe(graph, color)) {
			// Stampaj rezultat i vrati da je sve u redu
			printSolution(color);
			return true;
		}
		//ako smo dostigli kraj a bojenje nije bezbedno, onda nije sve u redu
		return false;
	}
	// proveravamo za svaku boju do maksimalnog broja boja, a to je m
	for (int j = 1; j <= m; j++) {
        //pretpostavimo da je boja tog cvora j i proverimo da li je moguce obojiti sa tom bojom
		color[i] = j;

		// pokrecemo ponovo algoritam, bojimo i ostale cvorove odgovarajucim bojama
        //ako je to bojenje moguce, onda smo nasli nase bojenje i vracamo true
        //i prekidamo algoritam
		if (graphColoring(graph, m, i + 1, color))
			return true;
        //u suprotnom nismo nasli bojenje, pa nastavljamo dalje i za cvor i biramo neku drugu boju, ali prvo moramo da vratimo predjasnje stanje, tj. da cvor i nema boju
		color[i] = 0;
	}
	return false;
}
/* Fja za stampanje resenja*/
void printSolution(vector<int> color)
{
    int max=-1;
	cout << "Resenje postoji! \n";
	for (int i = 0; i < V; i++){
		cout << i+1<<": " << color[i]<<endl;
        if(color[i]>max){
            max=color[i];
        }
        }
	cout <<"\nPotreban broj boja je: "<<max<< "\n";
}
int main(int argc, char** argv){
    for (int i = 1; i < argc; ++i){
        cout<<argv[i]<<endl;
        
        ifstream myFile;
        myFile.open(argv[i], ios::in);
        string line;
        
        //ucitavanje za E i N
        getline(myFile, line);
        istringstream ss(line);  
        
        int E;
        string x;
        ss>>x>>E>>V;
        //ucitavanje grana
        vector<int> U1;
        U1.resize(E);
        vector<int> V1;
        V1.resize(E);
        int k=0;
        while(getline( myFile, line) && k<E){
            istringstream ss(line); 
            string x;
            ss>>x>>U1[k]>>V1[k];
            k++;
        }
        myFile.close();
    vector<vector<bool>> graph;
        graph.resize(V);
        for(int j=0; j<V; j++){
            graph[j].resize(V);
        }
    int a,b;
    for(int i=0; i<E; i++){
      a=U1[i];
      b=V1[i];
      a--; b--;
      graph[a][b]=1;
      graph[b][a]=1;
    }

    for(int i=0; i<V; i++){
        for(int j=0; j<V; j++){
            cout<<graph[i][j]<<" ";
        }
        cout<<endl;
    }
    
	//inicijalizujemo sve boje na nula
	vector<int> color;
    color.resize(V);
	for (int i = 0; i < V; i++)
		color[i] = 0;
    
    int m = 1; // broj boja
	while(!graphColoring(graph, m, 0, color)){
        cout << "Resenje ne postoji za "<<m<<" boja"<<endl;
        m+=1;
        }
    }
    
	return 0;
}
