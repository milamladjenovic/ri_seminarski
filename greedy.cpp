#include<bits/stdc++.h>
#include<iostream>
using namespace std;
int i,j;
//matrica susedstva
vector<vector<int>> g;
//boje
vector<int> col;
//inicijalizacija
int N, E;
vector<int> U;
vector<int> V;
//kraj inicijalizacije

void greedyColoring()
{
    //boja prvog cvora je 0
   col[0] = 0;
   //svi ostali cvorovi imaju boju
   for (i=1;i<N;i++)
   col[i] = -1;
   
   //ovo su potencijalne boje, posto najvise mozemo imati
   //boja koliko imamo cvorova
   //a posto nijedna boja nije koriscena, stavljamo sve na 0(false)
   bool unuse[N];
   for (i=0;i<N;i++)
    unuse[i]=0;
   
   //obradjujemo ostale cvorove (prvi smo vec obradili)
   for (i = 1; i < N; i++)
{
      for (j=0;j<g[i].size();j++)            //susedi cvora i
        if (col[g[i][j]] != -1)              //cvor ima boju, nije -1
            unuse[col[g[i][j]]] = true;     //kazi da je ta boja iskoriscena 
      
      //prolazimo kroz sve boje i na prvu koj nije koriscena izlazimo iz petlje
      int cr;
      for (cr=0;cr<N;cr++)
         if (unuse[cr] == false)
            break;

     //kazemo da je to nasa nova boja
      col[i] = cr;
      //ovo radimo samo da bi ocistili niz unuse!!!
      //prolazimo kroz sve susede cvora i
      for (j=0;j<g[i].size();j++)
        if (col[g[i][j]] != -1)             //cvor ima boju, nije -1
            unuse[col[g[i][j]]] = false;   //ta boja nije iskoriscena
   }
}
int main(int argc, char** argv){
    for (int i = 1; i < argc; ++i){
        cout<<argv[i]<<endl;
        
        ifstream myFile;
        myFile.open(argv[i], ios::in);
        string line;
        
        //ucitavanje za E i N
        getline(myFile, line);
        istringstream ss(line);  
        
        int E;
        string x;
        ss>>x>>E>>N;
        //ucitavanje grana
        vector<int> U;
        U.resize(E);
        vector<int> V;
        V.resize(E);
        int k=0;
        while(getline( myFile, line) && k<E){
            istringstream ss(line); 
            string x;
            ss>>x>>U[k]>>V[k];
            k++;
        }
        myFile.close();
        g.resize(N);
        for(int j=0; j<N; j++){
            g[j].resize(N);
        }
        U.resize(N);
        V.resize(N);
        int a,b;
        col.resize(N);
        for(i=0;i<E;i++){
            a=U[i];
            b=V[i];
            a--; b--;
            g[a].push_back(b);
            g[b].push_back(a);
        }
        greedyColoring();
        int max=-1;
        for(i=0;i<N;i++){
            cout<<i+1<<": "<<col[i]+1<<"\n";
            if(col[i]+1>max){
                max=col[i]+1;}
        }
        cout<<"Ukupno boja: "<<max<<endl;
    }
   return 0;
}
