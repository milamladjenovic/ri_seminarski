#include <bits/stdc++.h>
#include <iostream>
#include <algorithm>
#include <fstream>
#include <string>
#include <cstring>
#include <sstream>
#include <time.h> 
using namespace std;

class node {
    //klasa koja civa boju i susede ovog cvora
public:
    //svi cvorovi imaju pocetnu boju 1
	int color = 1;
	set<int> edges;
};

int canPaint(vector<node>& nodes, int n, int m){

	//pravimo vektor posecenih cvorova i inicijalizujemo na 0
	vector<int> visited(n + 1, 0);

	//posto su svi cvorovi boje 1, maksimalna boja je 1
	int maxColors = 1;

    //Radimo BFS iz svih neposecenih pocetnih cvorova u slucaju da imamo nepovezan graf
    //maksimalno mozemo imati n komponenti u grafu jer ima n cvorova
	for (int sv = 1; sv <= n; sv++) {
        //ako je vec posecen samo nastavimo
		if (visited[sv])
			continue;
        //oznacimo da smo posetili cvor i stavimo ga u red
		visited[sv] = 1;
		queue<int> q;
		q.push(sv);

		// BFS obilzak
		while (!q.empty()) {
            //uzimamo cvor
			int top = q.front();
			q.pop();
            
            //obilazimo susede cvora
			for (auto it = nodes[top].edges.begin();it != nodes[top].edges.end(); it++){
                //ako je boja cvora ista kao i neka boja susednog cvora
                //povecati boju za 1 susednom cvoru
				if (nodes[top].color == nodes[*it].color)
					nodes[*it].color += 1;
                //ako broj boja nadmasuje m, onda ne mozemo obojiti graf sa m boja
				maxColors= max(maxColors, max(nodes[top].color,nodes[*it].color));
				if (maxColors > m)
					return 0;
                
                //ako susedni cvor nije posecen, obelezimo ga kao posecen i ubacimo u red
				if (!visited[*it]) {
					visited[*it] = 1;
					q.push(*it);
				}
			}
		}
	}
	return 1;
}
void printSolution(vector<node> nodes, int n, int m){
    for(int i=1; i<=n; i++){
        cout<<i<<": "<<nodes[i].color<<endl; 
    }
    cout<<"Potreban broj boja: "<<m<<endl;
}
int main(int argc, char** argv){
    for (int i = 1; i < argc; ++i){
        cout<<argv[i]<<endl;
        
        ifstream myFile;
        myFile.open(argv[i], ios::in);
        string line;
        
        //ucitavanje za E i N
        getline(myFile, line);
        istringstream ss(line);  
        
        int e, n;
        string x;
        ss>>x>>e>>n;
        //ucitavanje grana
        vector<int> U;
        U.resize(e);
        vector<int> V;
        V.resize(e);
        int k=0;
        while(getline( myFile, line) && k<e){
            istringstream ss(line); 
            string x;
            ss>>x>>U[k]>>V[k];
            k++;
        }
        myFile.close();
        int m=1;
        
        //pravi vektor od n+1 cvorova
        //nulta pozicija nije za citanje(zbog lepsih indeksa)
		vector<node> nodes(n + 1);

        //dodajemo susede
		for (int i = 0; i < e; i++) {
			int s=U[i];
			int d=V[i];
            
			nodes[s].edges.insert(d);
			nodes[d].edges.insert(s);
		}
		//pozivamo funkciju
		while(canPaint(nodes, n, m)==0){
            m++;
        }
        printSolution(nodes, n, m);
    }
	return 0;
}
